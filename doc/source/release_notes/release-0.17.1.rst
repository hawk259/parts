***************
Release  0.17.1
***************

* Fix packaging to use pyproject.toml
* Remove some files that did not merge correctly in 0.17.0 that caused some warnings. 
* Address issue with how SRC_DIR and PART_DIR handled to be more correct.
* Add better pattern to AutoMake builder source dependancies.
* Fix issue with copy script not being found with certain RHEL versions of python.
