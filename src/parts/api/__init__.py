from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from . import output
    from . import register
    from . import requirement
    from . import platforms